﻿using AmazonProductAdvertising;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Ulesanne.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            if (Request.QueryString["q"] != null)
            {
                if (Request.QueryString["q"].Length > 0)
                {
                    AmazonQuery search = new AmazonQuery();

                    string SearchIndex = "All";
                    string[] SearchResponseGroup = new string[] { "Medium" };
                    string Keywords = Request.QueryString["q"];

                    //Response is a ItemLookupResponse object.
                    var searchResponse = search.ItemSearch(SearchIndex, SearchResponseGroup, Keywords);

                    string[] titles = new string[10];
                    string[] images = new string[10];
                    string[] prices = new string[10];

                    for (int i = 0; i < searchResponse.Items[0].Item.Count(); i++)
                    {
                        titles[i] = searchResponse.Items[0].Item[i].ItemAttributes.Title;
                        images[i] = searchResponse.Items[0].Item[i].MediumImage.URL;
                        prices[i] = searchResponse.Items[0].Item[i].ItemAttributes.ListPrice.FormattedPrice;
                    }
         
                    ViewBag.titles = titles;
                    ViewBag.images = images;
                    ViewBag.prices = prices;
                }
            }
            

            return View();
        }
    }
}