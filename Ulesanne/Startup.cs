﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Ulesanne.Startup))]
namespace Ulesanne
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
